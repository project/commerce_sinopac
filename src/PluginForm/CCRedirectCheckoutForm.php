<?php

namespace Drupal\commerce_sinopac\PluginForm;

use Drupal\commerce_payment\PluginForm\PaymentOffsiteForm as BasePaymentOffsiteForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_payment\Exception\PaymentGatewayException;

/**
 * This is the redirect form lead the user to payment service.
 */
class CCRedirectCheckoutForm extends BasePaymentOffsiteForm {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;
    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $sinopac_plugin = $payment->getPaymentGateway()->getPlugin();

    $order = $payment->getOrder();
    $data = [];
    $sinopac_payment = $sinopac_plugin->createPaymentIntent($order, 'credit_card', $form['#return_url'], $form['#cancel_url']);

    if ($sinopac_payment->Status === 'F') {
      throw new PaymentGatewayException($this->t('Sinopac Payment Error, Message: @msg', [
        '@msg' => $sinopac_payment->Description,
      ]));
    }
    $rdtUrl = $sinopac_payment->CardParam->CardPayURL;

    return $this->buildRedirectForm(
    $form,
    $form_state,
    $rdtUrl,
    $data,
    self::REDIRECT_GET
    );
  }

}
