<?php

namespace Drupal\commerce_sinopac\Service;

/**
 * Provide the Sinopac related Class.
 */
class Sinopac {

  /**
   * Default API Sinopac object.
   *
   * @param string $mode
   *   This is the information needed for create sinopac api.
   * @param string $shopNo
   *   This is the information needed for create sinopac api.
   * @param string $hashA1
   *   This is the information needed for create sinopac api.
   * @param string $hashA2
   *   This is the information needed for create sinopac api.
   * @param string $hashB1
   *   This is the information needed for create sinopac api.
   * @param string $hashB2
   *   This is the information needed for create sinopac api.
   */
  public function __construct($mode, $shopNo, $hashA1, $hashA2, $hashB1, $hashB2) {
    $this->mode = $mode;
    $this->shopNo = $shopNo;
    $this->hash_key_a1 = $hashA1;
    $this->hash_key_a2 = $hashA2;
    $this->hash_key_b1 = $hashB1;
    $this->hash_key_b2 = $hashB2;
  }

  /**
   * Get transaction detail from Sinopac.
   *
   * @param string $remoteId
   *   OrderId we sent to sinopac, and we store this value in remote_id field.
   *
   * @return string
   *   Account No provided by the sinopac.
   */
  public function getTransactionDetail($remoteId) {
    $data['ShopNo'] = $this->shopNo;
    $data['OrderNo'] = $remoteId;
    $response = $this->apiService("OrderQuery", $data);
    if ($response->Status === "S") {
      return $response->OrderList[0];
    }
    else {
      return '';
    }

  }

  /**
   * Communicate with Sinopac api service.
   *
   * There are several api service provided.
   * 1. OrderCreate.
   * 2. OrderUnCapturedQuery.
   * 3. OrderMaintain.
   * 4. OrderQuery.
   * 5. OrderPayQuery.
   * 6. BillQuery.
   * 7. AllotQuery.
   *
   * @param string $service
   *   The API Service defined by the Sinopac.
   * @param array $data
   *   The data passed to Sinopac.
   */
  public function apiService($service, array $data) {

    $nonce = $this->getNonce();

    // Get HashId Value.
    $hashId = $this->getHashid();

    // Get iv.
    $iv = $this->getIv($nonce);

    // Get sign.
    $sign = $this->getSign($data, $nonce, $hashId);

    // Encrypt Message.
    $message = $this->encryptAesCbc(json_encode($data), $hashId, $iv);

    $mode = $this->mode;
    $shopNo = $this->shopNo;
    switch ($mode) {
      case 'test':
        $url = 'https://sandbox.sinopac.com/QPay.WebAPI/api/Order';
        break;

      case 'live':
        $url = 'https://funbiz.sinopac.com/QPay.WebAPI/api/Order';
        break;
    }

    $client = \Drupal::httpClient();
    $request = $client->post($url, [
      'json' => [
        'Version' => '1.0.0',
        'ShopNo' => $shopNo,
        'APIService' => $service,
        'Nonce' => $nonce,
        'Sign' => $sign,
        'Message' => $message,
      ],
    ]);
    $response = json_decode($request->getBody());

    // 取得 Response Nonce.
    $resNonce = $response->Nonce;

    // 取得 Response IV.
    $resIv = $this->getIv($resNonce);

    // 取得永豐銀行訊息內文.
    $result = $this->decryptAesCbc($response->Message, $hashId, $resIv);
    return json_decode($result);
  }

  /**
   * Get Nonce value from Sinopac payment provider.
   *
   * @var $shopNo
   *  shopNo id.
   */
  public function getNonce() {
    $mode = $this->mode;
    $shopNo = $this->shopNo;
    switch ($mode) {
      case 'test':
        $url = 'https://sandbox.sinopac.com/QPay.WebAPI/api/Nonce';
        break;

      case 'live':
        $url = 'https://funbiz.sinopac.com/QPay.WebAPI/api/Nonce';
        break;
    }
    $client = \Drupal::httpClient();
    $request = $client->post($url, [
      'json' => [
        'ShopNo' => $shopNo,
      ],
    ]);
    $response = json_decode($request->getBody());
    return $response->Nonce;
  }

  /**
   * Get Hash ID.
   *
   * @return string
   *   Return HashId value.
   */
  public function getHashId() {

    $a1 = $this->strToHexBytes($this->hash_key_a1);
    $a2 = $this->strToHexBytes($this->hash_key_a2);
    $b1 = $this->strToHexBytes($this->hash_key_b1);
    $b2 = $this->strToHexBytes($this->hash_key_b2);

    $xor1 = $this->setXor($a1, $a2);
    $xor2 = $this->setXor($b1, $b2);
    $result = $this->hexBytesToString($xor1) . $this->hexBytesToString($xor2);

    return $result;
  }

  /**
   * This is a helper function for getHashId.
   */
  public function strToHexBytes($string) {
    $hex = [];
    $j = 0;

    for ($i = 0; $i < strlen($string); $i += 2) {
      $hex[$j] = (int) base_convert(substr($string, $i, 2), 16, 10);

      $j += 1;
    }

    return $hex;
  }

  /**
   * This is a helper function for getHashId.
   */
  public function setXor($byte1, $byte2) {

    $result = [];

    for ($i = 0; $i < count($byte1); $i++) {
      $result[$i] = ($byte1[$i] ^ $byte2[$i]);
    }

    return $result;
  }

  /**
   * This is a helper function for getHashId.
   */
  public function hexBytesToString($hex) {
    $result = '';
    $str = '';

    for ($i = 0; $i < count($hex); $i++) {
      $str = (string) base_convert($hex[$i], 10, 16);

      if (strlen($str) < 2) {
        $str = '0' . $str;
      }

      $result .= $str;
    }

    return strtoupper($result);
  }

  /**
   * Encrype the string by sha256 method.
   *
   * @param string $string
   *   Provides the string want to encrype.
   */
  public function getIv($string) {
    $data = $this->sha256($string);
    return substr($data, strlen($data) - 16, 16);
  }

  /**
   * SHA256 後字串轉大寫.
   */
  public function sha256($data) {
    return strtoupper(hash('sha256', $data));
  }

  /**
   * Get the signed data.
   *
   * @param array $data
   *   The Data want to encrype.
   * @param string $nonce
   *   The nonce data provided by the Sinopac.
   * @param string $hashid
   *   The hashId generated according to the API doc.
   *
   * @return string
   *   Return signed data string.
   */
  public function getSign(array $data, $nonce, $hashid) {
    $result = '';
    $content = '';

    // Remove null.
    $data = array_filter((array) ($data));
    ksort($data);

    while ($fruit_name = current($data)) {
      if (is_array($data[key($data)]) == FALSE) {
        $content .= key($data) . '=' . $data[key($data)] . '&';
      }

      next($data);
    }
    $content = substr($content, 0, strlen($content) - 1);
    $content .= $nonce . $hashid;
    $result = $this->sha256($content);

    return $result;
  }

  /**
   * Encrype Data by using AES.
   *
   * @param string $data
   *   The data array want to send to sinopac.
   * @param string $key
   *   The key vlue is hashId.
   * @param string $iv
   *   The IV value is Hiv value.
   *
   * @return string
   *   Return the encryped string.
   */
  public function encryptAesCbc($data, $key, $iv) {
    $result = '';

    $padding = 16 - (strlen($data) % 16);
    $data .= str_repeat(chr($padding), $padding);
    $encrypt = openssl_encrypt($data, 'AES-256-CBC', $key, OPENSSL_RAW_DATA | OPENSSL_ZERO_PADDING, $iv);

    $result = strtoupper(bin2hex($encrypt));

    return $result;
  }

  /**
   * Decryp Data by using AES Method.
   *
   * @param string $data
   *   The data message want to decrypt.
   * @param string $key
   *   The key vlue is hashId.
   * @param string $iv
   *   The IV value is Hiv value.
   *
   * @return string
   *   The decrypt string.
   */
  public function decryptAesCbc($data, $key, $iv) {
    $result = '';

    // 支援 PHP 5 >= 5.4.0, PHP 7
    // $encrypt = hex2bin($data);
    // 支援 PHP 4, PHP 5, PHP 7.
    $encrypt = pack('H*', $data);

    // 因有客戶反應解密後字串被截斷, 查後發現有可能是 OPENSSL_ZERO_PADDING 客戶拿掉造成取字串長度錯誤造成.
    $result = openssl_decrypt($encrypt, 'AES-256-CBC', $key, OPENSSL_RAW_DATA, $iv);
    return $result;
  }

  /**
   * Transfer pay status code to message.
   *
   * @param string $code
   *   The return code from Sinopac.
   *
   * @return string
   *   Transfer code to message.
   */
  public function transferPayStatus($code) {

    $matchTable = [
      '1A200' => '待付款',
      '1A250' => '帳號逾期',
      '1A400' => '付款完成',
      '1A900' => '已撥款',
      '1C200' => '待付款',
      '1C250' => '刷卡逾期',
      '1C300' => '已授權未請款',
      '1C350' => '授權失效',
      '1C351' => '取消授權',
      '1C400' => '請款完成',
      '1C900' => '已撥款',
    ];

    return !isset($matchTable[$code]) ? $code : $matchTable[$code];

  }

}
