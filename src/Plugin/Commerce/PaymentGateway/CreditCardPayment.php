<?php

namespace Drupal\commerce_sinopac\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Entity\PaymentInterface;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_price\Price;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\commerce_payment\Exception\PaymentGatewayException;
use Drupal\commerce_sinopac\Service\Sinopac;

/**
 * Provides the Sinopac Credit Card offsite Checkout payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "sinopac_credit_card",
 *   label = @Translation("Sinopac Credit Card"),
 *   display_label = @Translation("Credit Card"),
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_sinopac\PluginForm\CCRedirectCheckoutForm",
 *   },
 * )
 */
class CreditCardPayment extends OffsitePaymentGatewayBase implements CreditCardPaymentInterface {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The sinopac api object.
   *
   * @var \Drupal\commerce_sinopac\Service\Sinopac
   */
  protected $sinopac;

  /**
   * Constructs a new OffsitePayPlug object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\commerce_payment\PaymentTypeManager $payment_type_manager
   *   The payment type manager.
   * @param \Drupal\commerce_payment\PaymentMethodTypeManager $payment_method_type_manager
   *   The payment method type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, EntityTypeManagerInterface $entity_type_manager, PaymentTypeManager $payment_type_manager, PaymentMethodTypeManager $payment_method_type_manager, TimeInterface $time) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time);
    $this->entityTypeManager = $entity_type_manager;
    $this->init();
  }

  /**
   * Re-initializes the SDK after the plugin is unserialized.
   */
  public function __wakeup() {
    $this->init();
  }

  /**
   * Initializes the SDK.
   */
  protected function init() {
    $this->sinopac = new Sinopac(
        $this->configuration['mode'],
        $this->configuration['shop_no'],
        $this->configuration['hash_key_a1'],
        $this->configuration['hash_key_a2'],
        $this->configuration['hash_key_b1'],
        $this->configuration['hash_key_b2']
    );
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
        $configuration,
        $plugin_id,
        $plugin_definition,
        $container->get('entity_type.manager'),
        $container->get('plugin.manager.commerce_payment_type'),
        $container->get('plugin.manager.commerce_payment_method_type'),
        $container->get('datetime.time')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'shop_no' => '',
      'hash_key_a1' => '',
      'hash_key_a2' => '',
      'hash_key_b1' => '',
      'hash_key_b2' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    $form['shop_no'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Shop No'),
      '#description' => $this->t('This is the Shop No from the Sinopac manager.'),
      '#default_value' => $this->configuration['shop_no'],
      '#required' => TRUE,
    ];

    $form['hash_key_a1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key A1'),
      '#description' => $this->t('This is the Hash Key A1 from the Sinopac manager.'),
      '#default_value' => $this->configuration['hash_key_a1'],
      '#required' => TRUE,
    ];

    $form['hash_key_a2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key A2'),
      '#description' => $this->t('This is the Hash Key A2 from the Sinopac manager.'),
      '#default_value' => $this->configuration['hash_key_a2'],
      '#required' => TRUE,
    ];

    $form['hash_key_b1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key B1'),
      '#description' => $this->t('This is the Hash Key B1 from the Sinopac manager.'),
      '#default_value' => $this->configuration['hash_key_b1'],
      '#required' => TRUE,
    ];

    $form['hash_key_b2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Hash Key B2'),
      '#description' => $this->t('This is the Hash Key B2 from the Sinopac manager.'),
      '#default_value' => $this->configuration['hash_key_b2'],
      '#required' => TRUE,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);
      $this->configuration['shop_no'] = $values['shop_no'];
      $this->configuration['hash_key_a1'] = $values['hash_key_a1'];
      $this->configuration['hash_key_a2'] = $values['hash_key_a2'];
      $this->configuration['hash_key_b1'] = $values['hash_key_b1'];
      $this->configuration['hash_key_b2'] = $values['hash_key_b2'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createPaymentIntent(OrderInterface $order, $paymentType = "credit_card", $returnUrl = NULL, $cancelUrl = NULL) {

    $data['PayType'] = 'C';
    $data['PrdtName'] = 'Credit Card Order';
    $data['CardParam']['AutoBilling'] = 'Y';
    $remoteId = $data['PayType'] . date("YmdHis");
    $data['ShopNo'] = $this->configuration['shop_no'];
    $data['Amount'] = round($order->getTotalPrice()->getNumber() * 100);
    $data['CurrencyID'] = 'TWD';
    $data['OrderNo'] = $remoteId;
    $data['ReturnURL'] = $returnUrl;
    $data['BackendURL'] = $this->getNotifyUrl()->toString();

    $indent = $this->sinopac->apiService("OrderCreate", $data);

    if ($indent->Status === "F") {
      throw new PaymentGatewayException('Payment authorized failed.');
    }
    // Establish payment entity.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $payment = $payment_storage->create([
      'state' => 'authorization',
      'amount' => new Price($order->getTotalPrice()->getNumber(), 'TWD'),
      'payment_gateway' => $this->entityId,
      'order_id' => $order->id(),
      'test' => $this->getMode() == 'test',
      'remote_id' => $remoteId,
      'authorized' => $this->time->getRequestTime(),
    ]);
    $payment->save();

    return $indent;
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    $shopNo = $request->get('ShopNo');
    $payToken = $request->get('PayToken');

    $data['ShopNo'] = $shopNo;
    $data['PayToken'] = $payToken;

    $transaction = $this->sinopac->apiService('OrderPayQuery', $data);

    if ($transaction->Status === 'F') {
      throw new PaymentGatewayException('Require Payment Detail Error.');
    }

    if ($transaction->Status === 'S') {
      $tsResultContent = $transaction->TSResultContent;
      $remoteId = $tsResultContent->OrderNo;
      // Establish payment entity.
      $payments = $this->entityTypeManager->getStorage('commerce_payment')->loadByProperties([
        'remote_id' => $remoteId,
      ]);
      $paymentId = array_keys($payments)[0];
      $payment = $payments[$paymentId];

      if ($tsResultContent->Status === 'F') {
        // Payment failed, we can create commerce payment at here.
        $payment->setState('authorization_voided');
        $payment->save();
        \Drupal::messenger()->addError(t('Authorization Voided'));
        throw new PaymentGatewayException($this->t('Order ID: @orderid, Payment Remote ID: @remote_id, Message: @msg', [
          '@orderid' => $order->id(),
          '@remote_id' => $tsResultContent->OrderNo,
          '@msg' => $tsResultContent->Description,
        ]));
      }
      // Credit Card Payment Method.
      if ($tsResultContent->Status === 'S') {
        $payment->setState('completed');
        $payment->save();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function capturePayment(PaymentInterface $payment, Price $amount = NULL) {
  }

  /**
   * {@inheritdoc}
   */
  public function voidPayment(PaymentInterface $payment) {
  }

  /**
   * {@inheritdoc}
   */
  public function refundPayment(PaymentInterface $payment, Price $amount = NULL) {
  }

}
