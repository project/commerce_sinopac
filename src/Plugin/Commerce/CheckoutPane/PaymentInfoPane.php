<?php

namespace Drupal\commerce_sinopac\Plugin\commerce\CheckoutPane;

use Drupal\commerce_checkout\Plugin\Commerce\CheckoutPane\CheckoutPaneBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_sinopac\Service\Sinopac;

/**
 * Provides a Custom Ksnews Shopping pane.
 *
 * @CommerceCheckoutPane(
 *   id = "commerce_sinopac_paymentinfo_pane",
 *   label = @Translation("Sinopac Payment information pane."),
 * )
 */
class PaymentInfoPane extends CheckoutPaneBase {

  /**
   * {@inheritdoc}
   */
  public function buildPaneForm(array $pane_form, FormStateInterface $form_state, array &$complete_form) {
    $pane_form["#theme"] = 'commerce_sinopac_payment_pane';
    $complete_form["commerce_sinopac_paymentinfo_pane"]["#theme"] = 'commerce_sinopac_payment_pane';

    $order = $this->order;
    $id = $order->id();

    $payments = \Drupal::entityTypeManager()->getStorage('commerce_payment')->loadByProperties([
      'order_id' => $id,
    ]);
    $paymentId = array_keys($payments)[0];
    $payment = $payments[$paymentId];

    $remoteID = $payment->getRemoteId();
    $payment_gateway = $payment->getPaymentGateway();
    $configuration = $payment_gateway->getPluginConfiguration();

    $sinopac = new Sinopac(
      $payment->getPaymentGatewayMode(),
      $configuration['shop_no'],
      $configuration['hash_key_a1'],
      $configuration['hash_key_a2'],
      $configuration['hash_key_b1'],
      $configuration['hash_key_b2']
    );

    $transactionInfo = $sinopac->getTransactionDetail($remoteID);

    if ($transactionInfo->PayType === 'A') {
      $atmNo = $transactionInfo->ATMParam->AtmPayNo;
      $payStatus = $transactionInfo->PayStatus;
      $pane_form['#account_no'] = $atmNo;
      $pane_form['#pay_status'] = $sinopac->transferPayStatus($payStatus);
      $pane_form['message'] = [
        '#markup' => $this->t('This is the information'),
      ];
    }

    return $pane_form;
  }

}
